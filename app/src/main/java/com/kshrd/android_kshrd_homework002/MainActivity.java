package com.kshrd.android_kshrd_homework002;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btn_click_gallery(View view) {
        Intent galleryIntent = new Intent(this,GalleryActivity.class);
        startActivity(galleryIntent);
    }

    public void btn_click_contact(View view) {
        Intent contactIntent = new Intent(this, ContactActivity.class);
        startActivity(contactIntent);
    }

    public void btn_click_notebook(View view) {
        Intent notebookIntent = new Intent(this,NoteBookActivity.class);
        startActivity(notebookIntent);
    }

    public void btn_click_profile(View view) {
        Intent profileIntent = new Intent(this,ProfileActivity.class);
        startActivity(profileIntent);
    }

    public void btn_click_call(View view) {
        Intent profileIntent = new Intent(this,CallActivity.class);
        startActivity(profileIntent);
    }
}