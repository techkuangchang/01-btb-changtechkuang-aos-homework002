package com.kshrd.android_kshrd_homework002;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SaveActivity extends AppCompatActivity {

    TextView profileName, profileGender, profileBirthday, profilePosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);

        profileName = findViewById(R.id.profileName);
        profileGender = findViewById(R.id.profileGender);
        profileBirthday = findViewById(R.id.profileBirthday);
        profilePosition = findViewById(R.id.profilePosition);

        String getProfileName = getIntent().getStringExtra("profileName");
        String getGender = getIntent().getStringExtra("gender");
        String getBirthday = getIntent().getStringExtra("birthday");
        String getDeveloper = getIntent().getStringExtra("developer");

        profileName.setText(getProfileName);
        profileGender.setText(getGender);
        profileBirthday.setText(getBirthday);
        profilePosition.setText(getDeveloper);
    }
}