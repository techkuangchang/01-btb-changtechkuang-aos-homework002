package com.kshrd.android_kshrd_homework002;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

public class NoteBookActivity extends AppCompatActivity {

    TextView mTxtTitle, mTxtContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notebook);

        String getTitleIntent = getIntent().getStringExtra("titleKey");
        String getContentIntent = getIntent().getStringExtra("contentKey");

        mTxtTitle = findViewById(R.id.txtTitle);
        mTxtContent = findViewById(R.id.txtContent);

        mTxtTitle.setMaxLines(1);
        mTxtTitle.setEllipsize(TextUtils.TruncateAt.END);

        mTxtContent.setMaxLines(2);
        mTxtTitle.setEllipsize(TextUtils.TruncateAt.END);

        Log.i(getTitleIntent,"Title is work");
        Log.i(getContentIntent,"Content is work");
//        mTxtTitle.setText(getTitleIntent);
//        mTxtContent.setText(getContentIntent);
    }

    public void btn_click_add(View view) {
        Intent addIntent = new Intent(this,SubNoteBookActivity.class);
        startActivity(addIntent);
    }

}