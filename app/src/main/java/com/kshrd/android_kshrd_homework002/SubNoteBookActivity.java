package com.kshrd.android_kshrd_homework002;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.TextView;

public class SubNoteBookActivity extends AppCompatActivity {

    EditText mTxtTitle, mTxtContent;
    Button mBtnSave, mBtnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_note_book);
    }

    public void btn_cancel(View view) {

    }

    public void btn_save(View view) {
        Intent btnSaveIntent = new Intent(this,NoteBookActivity.class);

        mTxtTitle = findViewById(R.id.editTxtTitle);
        mTxtContent = findViewById(R.id.editTxtContent);

        String inputTitle = this.mTxtTitle.getText().toString();
        String inputContent = this.mTxtContent.getText().toString();

        //Pass data to NoteBookActivity
        btnSaveIntent.putExtra("titleKey",inputTitle);
        btnSaveIntent.putExtra("contentKey",inputContent);

        //Start Activity
        startActivity(btnSaveIntent);
    }
}