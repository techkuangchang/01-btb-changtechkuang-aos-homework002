package com.kshrd.android_kshrd_homework002;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.Calendar;

public class ProfileActivity extends AppCompatActivity {

    DatePickerDialog datePicker;
    Button dateBtn, btn_profile_save;
    TextView dateTxt;
    TextView editTxtProfileName;
    String devTxt;
    Spinner spinner;
    RadioButton radioGenderBtn;
    RadioGroup radioGenderGroup;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        dateTxt = findViewById(R.id.txtView);
        dateBtn = findViewById(R.id.btnClick);

        //Spinner Dropdown
        spinner = findViewById(R.id.spinner);

        //Save Button to another activity
        btn_profile_save = findViewById(R.id.btn_profile_save);
        editTxtProfileName = findViewById(R.id.editTxtProfileName);
        radioGenderGroup = findViewById(R.id.radioSex);

        String[] date = {"Web Developer","IOS Developer","Android Developer","Data Analyzer","Web Designer"};

        //Date Picker
        dateBtn.setInputType(InputType.TYPE_NULL);
        dateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                //date picker dialog
                datePicker = new DatePickerDialog(ProfileActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                dateTxt.setText(dayOfMonth + "/" + (month) + "/" + year);
                            }
                        },year,month,day);
                datePicker.show();
            }
        });

        //Spinner
        ArrayAdapter<String> dateAdapter = new ArrayAdapter<>(
                ProfileActivity.this,
                android.R.layout.simple_spinner_dropdown_item,
                date
        );

        spinner.setAdapter(dateAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getItemAtPosition(position).toString();
//                Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
                devTxt = text;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Button Save

        btn_profile_save.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                int selected = radioGenderGroup.getCheckedRadioButtonId();

                radioGenderBtn = findViewById(selected);

                Intent saveIntent = new Intent(ProfileActivity.this,SaveActivity.class);
                saveIntent.putExtra("profileName",editTxtProfileName.getText().toString());
                saveIntent.putExtra("gender",radioGenderBtn.getText());
                saveIntent.putExtra("birthday",dateTxt.getText());
                saveIntent.putExtra("developer",devTxt);
                startActivity(saveIntent);
            }
        });

    }
}